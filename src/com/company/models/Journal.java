package com.company.models;

public class Journal {
    private String title;
    private String subject;
    private int year;
    private String mounth;

    public Journal(String title, String subject, int year, String mounth) {
        this.title = title;
        this.subject = subject;
        this.year = year;
        this.mounth = mounth;
    }

    public int getYear() {
        return year;
    }

    public void print() {
        System.out.println("\"" + title + "\" #" + subject);
        System.out.println(mounth + ", " + year);
        System.out.println("_____");
    }
}
