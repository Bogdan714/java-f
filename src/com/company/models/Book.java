package com.company.models;

public class Book {
    private String title;
    private String author;
    private String publisher;
    private int year;

    public Book(String title, String author, String publisher, int year) {
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public void print() {
        System.out.println("\"" + title + "\" (" + author + ")");
        System.out.println("publisher: " + publisher + ", " + year);
        System.out.println("_____");
    }

}
