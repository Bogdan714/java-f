package com.company.models;

public class Bank {
    private String name;
    private float course;
    private float commission;
    private float limit;

    public Bank(String name, float course, float limit) {
        this.name = name;
        this.course = course;
        this.commission = 0.05f;
        this.limit = limit;
    }

    public float change(float value){
        if(value > limit){
            return -1;
        } else {
            return value / course * (1 - commission);
        }
    }

    public String getName() {
        return name;
    }

    public float getCourse() {
        return course;
    }

    public float getCommission() {
        return commission;
    }

    public float getLimit() {
        return limit;
    }
}
