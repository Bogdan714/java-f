package com.company.models;

public class Yearbook {
    private String title;
    private String subject;
    private String publisher;
    private int year;

    public Yearbook(String title, String subject, String publisher, int year) {
        this.title = title;
        this.subject = subject;
        this.publisher = publisher;
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public void print() {
        System.out.println("\"" + title + "\" #" + subject);
        System.out.println("publisher: " + publisher + ", " + year);
        System.out.println("_____");
    }
}
