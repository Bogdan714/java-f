package com.company.models;

public class BlackMarket {
    private String name;
    private float course;

    public BlackMarket() {
    }

    public BlackMarket(String name, float course) {
        this.name = name;
        this.course = course;
    }

    public float change(float value) {
        return value / course;
    }

    public String getName() {
        return name;
    }

    public float getCourse() {
        return course;
    }
}
