package com.company.additional;

import com.company.models.Bank;
import com.company.models.BlackMarket;
import com.company.models.Exchanger;

public class Finance {
    private Bank[] banks;
    private BlackMarket[] blackMarkets;
    private Exchanger[] exchangers;
    Object[] types = new Object[3];

    public Finance(int banks, int exchangers, int blackMarkets) {
        this.banks = Generator.generateBanks(banks);
        this.blackMarkets = Generator.generateBlackMarkets(blackMarkets);
        this.exchangers = Generator.generateExchangers(exchangers);
        types[0] = this.banks[0];
        types[1] = this.blackMarkets[0];
        types[2] = this.exchangers[0];
    }

    public void printCapable(double amount) {
        System.out.println("Capable organizations: ");
        for (Exchanger exchanger : exchangers) {
            if (exchanger.getLimit() >= amount) {
                System.out.println(exchanger.getName());
            }
        }
        for (Bank bank : banks) {
            if (bank.getLimit() >= amount) {
                System.out.println(bank.getName());
            }
        }
        System.out.println("Else you can exchange your money in any black market:");
        for (BlackMarket blackMarket : blackMarkets) {
            System.out.println(blackMarket.getName());
        }
    }

    public float exchange(float money, String name) {
        float result = 0;
        for (Bank bank : banks) {
            if (bank.getName().equalsIgnoreCase(name)) {
                result = bank.change(money);
                break;
            }
        }
        for (Exchanger exchanger : exchangers) {
            if (exchanger.getName().equalsIgnoreCase(name)) {
                result = exchanger.change(money);
                break;
            }
        }
        for (BlackMarket blackMarket : blackMarkets) {
            if (blackMarket.getName().equalsIgnoreCase(name)) {
                result = blackMarket.change(money);
                break;
            }
        }
        return result;
    }
}
