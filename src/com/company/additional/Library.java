package com.company.additional;

import com.company.models.Book;
import com.company.models.Journal;
import com.company.models.Yearbook;

public class Library {
    Book[] books;
    Journal[] journals;
    Yearbook[] yearbooks;

    public Library(int books, int journals, int yearbooks) {
        this.books = Generator.generateBooks(books);
        this.journals = Generator.generateJournals(journals);
        this.yearbooks = Generator.generateYearbooks(yearbooks);
    }

    public void printByYear(int year) {
        boolean found = false;
        for (Book book : books) {
            if (book.getYear() == year) {
                book.print();
                found = true;
            }
        }
        for (Journal journal : journals) {
            if (journal.getYear() == year) {
                journal.print();
                found = true;
            }
        }
        for (Yearbook yearbook : yearbooks) {
            if (yearbook.getYear() == year) {
                yearbook.print();
                found = true;
            }
        }
        if (!found) {
            System.out.println("Materials not found");
        }
    }
}
