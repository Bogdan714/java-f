package com.company.additional;

import com.company.models.Yearbook;
import com.company.models.*;

public class Generator {
    public static Bank[] generateBanks(int number) {
        Bank[] banks = new Bank[number];
        for (int i = 0; i < banks.length; i++) {
            banks[i] = new Bank("Bank " + i, 25 + ((float) Math.random() * 4) - 2, 150000);
        }
        return banks;
    }

    public static Exchanger[] generateExchangers(int number) {
        Exchanger[] exchangers = new Exchanger[number];
        for (int i = 0; i < exchangers.length; i++) {
            exchangers[i] = new Exchanger("Exchanger " + i, 26 + ((float) Math.random() * 4) - 2, 1000 + (float) (Math.random() * 1000));
        }
        return exchangers;
    }

    public static BlackMarket[] generateBlackMarkets(int number) {
        BlackMarket[] blackMarkets = new BlackMarket[number];
        for (int i = 0; i < blackMarkets.length; i++) {
            blackMarkets[i] = new BlackMarket("BM " + i, 29 + ((float) Math.random() * 4) - 2);
        }
        return blackMarkets;
    }

    public static Book[] generateBooks(int number) {
        Book[] books = new Book[number];
        for (int i = 0; i < books.length; i++) {
            books[i] = new Book("Book " + i, "Author " + i, "publishing house " + i, 2000 + i);
        }
        return books;
    }

    public static Journal[] generateJournals(int number) {
        Journal[] journals = new Journal[number];
        for (int i = 0; i < journals.length; i++) {
            journals[i] = new Journal("Journal " + i, "News " + i, 2000 + i, "Jun");
        }
        return journals;
    }

    public static Yearbook[] generateYearbooks(int number) {
        Yearbook[] yearbooks = new Yearbook[number];
        for (int i = 0; i < yearbooks.length; i++) {
            yearbooks[i] = new Yearbook("Yearbook " + i, "News", "publishing house " + i, 2000 + i);
        }
        return yearbooks;
    }
}
