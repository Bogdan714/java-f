package com.company;

import com.company.additional.Finance;
import com.company.additional.Library;

import java.util.Scanner;

public class Main {
    public static final int N = 10;

    public static void main(String[] args) {
        //task 1
        System.out.println(1);
        //init scanner
        Scanner scan = new Scanner(System.in);
        //init finance
        Finance market = new Finance(5, 5, 5);
        //enter amount of money
        System.out.println("Enter the amount of money: ");
        int amount = Integer.parseInt(scan.nextLine());
        //looking for services
        market.printCapable(amount);
        //enter name of the bank
        System.out.println("Enter the bank, exchanger or black market:");
        String name = scan.nextLine();
        float change = market.exchange(amount, name);
        if (change >= 0) {
            System.out.println(String.format("you money now: $%.2f", change));
        } else {
            System.out.println("Sorry, something went wrong :(");
        }

        //task 2
        System.out.println(2);
        //init library
        Library library = new Library(18, 18, 18);
        System.out.println("Materials for which year(2000 - 2017) you want to see?");
        //input year
        int year = Integer.parseInt(scan.nextLine());
        System.out.println("Materials for " + year + ":");
        System.out.println("____________________");
        //result
        library.printByYear(year);
    }
}